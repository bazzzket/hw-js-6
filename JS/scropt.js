class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }

    getSalary() {
        document.write(`Заробітня плата працівника ${this.name} ${this.surname}: ` + this.rate * this.days + "<br>");
    };
}

class MyString {
    reverse(string) {
        return string.split("").reverse().join("");
    }

    ucFirst(string) {
        return string[0].toUpperCase() + string.slice(1, string.length);
    }

    ucWords(string) {
        let result = "";
        for (let word of string.split(" ")) {
            result += word[0].toUpperCase() + word.slice(1, word.length) + " ";
        }
        return result.slice(0, result.length-1);
    }
}

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
}

class Car{
    constructor(brand, carClass, weight, engine, driver) {
        this.brand = brand; 
        this.carClass = carClass;
        this.weight = weight;
        this.engine = engine;
        this.driver = driver;
    }
    
    start() {document.write("Поїхали <br>");}
    stop() {document.write("Зупиняємося <br>");}
    turnRight() {document.write("Поворот праворуч <br>");}
    turnLeft() {document.write("Поворот ліворуч <br>");}

    /*toString() {
        document.write("<hr>");
        document.write("<b>" + this.constructor.name + "</b><br>");                
        for (let prop in this) {
            if (typeof this[prop] === "function") continue;
            if (typeof this[prop] === "object") {
                document.write("<br><b>" + this[prop].constructor.name + "</b><br>");                
                for (let propOfInnerObject in this[prop]) {
                    document.write(propOfInnerObject + ": " + this[prop][propOfInnerObject] + "<br>");         
                }
            } else {
                document.write(prop + ": " + this[prop] + "<br>");
            }
        }
    }*/

    toString() {
        let str = "";
        let innerStr = "";
        for (let prop in this) {
            if (typeof this[prop] === "function") continue;
            if (typeof this[prop] === "object") {               
                for (let propOfInnerObject in this[prop]) {
                    innerStr += propOfInnerObject + ": " + this[prop][propOfInnerObject] + "; ";
                }
            } else {
                str += prop + ": " + this[prop] + "; ";
            }
            str += innerStr;
            innerStr = "";
        }
        document.write(str + "<br><br>");
    }
}

class Driver{
    constructor(fullName, drivingExperience) {
        this.fullName = fullName; 
        this.drivingExperience = drivingExperience;
    }
}

class Engine {
    constructor(power, manufacturer) {
        this.power = power; 
        this.manufacturer = manufacturer;
    }
}

class Lorry extends Car {
    constructor(brand, carClass, weight, engine, driver, loadСapacity) {
        super(brand, carClass, weight, engine, driver);
        this.loadСapacity = loadСapacity;
    }   
}

class SportCar extends Car {
    constructor(brand, carClass, weight, engine, driver, maxSpeed) {
        super(brand, carClass, weight, engine, driver);
        this.maxSpeed = maxSpeed;
    }   
}

class Aminal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }
    makeNoise() {console.log(`Тварина здає звуки`)}
    eat() {console.log(`Тварина йсть корм`)}
    sleep() {console.log(`Тварина спить`)}
}

class Dog extends Aminal {
    constructor(food, location, isHunting) {
        super(food, location);
        this.isHunting = isHunting;     // чи є вона мисливською
    }
    makeNoise() {return `Собака гавкає`}
    eat() {return `Собака їсть ${this.food}`}
}

class Cat extends Aminal {
    constructor(food, location, mousesCatched) {
        super(food, location)
        this.mousesCatched = mousesCatched;
    }
    makeNoise() {return `Кіт мурчить`}
    eat() {return `Кіт їсть ${this.food}`}
}

class Horse extends Aminal {
    constructor(food, location, isTamed) {
        super(food, location)
        this.isTamed = isTamed;         // чи приручена
    }
    makeNoise() {return(`Коняка ірже`)}
    eat() {return `Коняка їсть ${this.food}`}
}

class Vet {
    treatAnimal(animal) {
        console.log(animal.eat());
        console.log(animal.makeNoise());
        switch (animal.constructor.name){
            case "Dog": console.log("Собака живе у " + animal.location); break;
            case "Cat": console.log("Кіт живе у " + animal.location); break;
            case "Horse": console.log("Коняка живе у " + animal.location);
        }
        console.log("");
    }
}

// ------------------------------------------Працівник--------------------------------------------

let workers = [];

do {
    workers.push(
        new Worker(
            prompt("Введіть ім'я працівника:"),
            prompt("Введіть прізвище працівника:"),
            prompt("Введіть його ставку за день роботи:"),
            prompt("Введіть кількість відпрацьованих днів:")
        )    
    ); 
} while (confirm("Додати працівника?"))

for (let idx of workers) {
    idx.getSalary();
}

document.write("<hr>");

// --------------------------------------------Строка--------------------------------------------

let str = prompt("Введіть будь-яку строку з декількох слів:");
document.write("Ви ввели: " + str + "<br>");
document.write("Перевертаємо строку: " + MyString.prototype.reverse(str) + "<br>");
document.write("Робимо першу літеру заглавною: " + MyString.prototype.ucFirst(str) + "<br>");
document.write("Робимо першу літеру кожного слова заглавною: " + MyString.prototype.ucWords(str) + "<br>");

document.write("<hr>");

// --------------------------------------------Телефон-------------------------------------------

const phones = [];
phones.push(new Phone(380507163322, "Samsung A50", 180)); 
phones.push(new Phone(380673153318, "iPhone 14", 200));
phones.push(new Phone(380986451844, "Redmi Note 11", 215));

const callers = ["Зеленський", "Порошенко", "Путін хуйло"];

for (let phone of phones) {
    for (let prop in phone) {
       console.log(prop + ": " + phone[prop]); 
    }
    console.log("");
}

Phone.prototype.receiveCall = function (name) {
    console.log("Телефонує " + name);
}

Phone.prototype.getNumber = function () {
     return this.number;
}

for (let i = 0; i < phones.length; i++) {
    phones[i].receiveCall(callers[i]);
    console.log(phones[i].getNumber());
    console.log("");  
}

// --------------------------------------------Автомобілі-------------------------------------------

var myCar = new Car ("VolksWagen", "S", 1500, new Engine(180, "VW"), new Driver("Вася aka Шумахер", 18));
var myLorry = new Lorry ("VolksWagen", "H", 3000, new Engine(180, "VW"), new Driver("Валера Однорукий", 2), 5000);
var mySportCar = new SportCar ("Ferrary", "S", 1200, new Engine(400, "Ferrary"), new Driver("Доминик Торетто", 10), 350);

myCar.toString();
myLorry.toString();
mySportCar.toString();

Driver.prototype.toString = Engine.prototype.toString = Car.prototype.toString;

let myEngine = new Engine(160, "Hyundai");
let driver = new Driver("Юлія Тімошенко", 50)

myEngine.toString();
driver.toString();

// -----------------------------------------Тварини і ветеринар-------------------------------------

let animals = [
               new Dog("м'ясо", "домі", true), 
               new Cat("сметану", "кімнаті", 5),
               new Horse("сіно", "полі", false)
              ];

for (let animal of animals) {
    Vet.prototype.treatAnimal(animal);
}

document.write("<br><br><b> Інші дані виводяться у консоль, як зазначене у завданні </b>")
